# - Try to find RobotControllerInterface
# Once done this will define
#
#  RobotControllerInterface_FOUND - system has RobotControllerInterface
#  RobotControllerInterface_INCLUDE_DIRS - the RobotControllerInterface include directory
#  RobotControllerInterface_LIBRARIES - Link these to use RobotControllerInterface
#  RobotControllerInterface_DEFINITIONS - Compiler switches required for using RobotControllerInterface
#
#  Copyright (c) 2016 Shehzad Ahmed <shehzi001@gmail.com>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (RobotControllerInterface_LIBRARIES AND RobotControllerInterface_INCLUDE_DIRS)
  # in cache already
  set(RobotControllerInterface_FOUND TRUE)
else (RobotControllerInterface_LIBRARIES AND RobotControllerInterface_INCLUDE_DIRS)

  find_library(RobotControllerInterface_LIBRARY
    NAMES
      robot_controller_interface
    PATHS
      /usr/lib/robot_controller_interface/
  )

  set(RobotControllerInterface_INCLUDE_DIRS
    /usr/include/
  )

  if (RobotControllerInterface_LIBRARY)
    set(RobotControllerInterface_LIBRARIES
        ${RobotControllerInterface_LIBRARIES}
        ${RobotControllerInterface_LIBRARY}
    )
  endif (RobotControllerInterface_LIBRARY)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(RobotControllerInterface DEFAULT_MSG RobotControllerInterface_LIBRARIES RobotControllerInterface_INCLUDE_DIRS)

  # show the RobotControllerInterface_INCLUDE_DIRS and RobotControllerInterface_LIBRARIES variables only in the advanced view
  mark_as_advanced(RobotControllerInterface_INCLUDE_DIRS RobotControllerInterface_LIBRARIES)

endif (RobotControllerInterface_LIBRARIES AND RobotControllerInterface_INCLUDE_DIRS)

