#!/usr/bin/env python

'''
An example of joint state publisher.

Author: Shehzad Ahmed
Date: 29.03.2016
'''

import time
# Import the modules we need
from qpid.messaging import *
from zeno_data_communication_manager.data_communication_manager import *


def position_cmd_cb(MSG):
    #time.sleep(2.0)
    print MSG
    print '=================================='

if __name__ == "__main__":
    broker = "tcp://localhost:5672"
    connection_options = {'sasl_mechanisms':'ANONYMOUS PLAIN MD5',
                          'username':'admin',
                          'password':'admin'}
    zeno_py = Node()
    zeno_py.init_node("joint_states_pub", broker,
                      connection_options=connection_options)

    joint_states_pub = Publisher(zeno_py, "/joint_controller/joint_states", "amq.topic", 1)
    
    position_command_sub = Subscriber(zeno_py, "/joint_controller/joint_states", "amq.topic", 1, position_cmd_cb)

    joint_names = ["neck_roll", "neck_pitch", "neck_yaw", "waist",
            "left_shoulder_pitch", "left_shoulder_roll",
            "left_elbow_roll", "left_elbow_yaw",
            "right_shoulder_pitch", "right_shoulder_roll",
            "right_elbow_roll", "right_elbow_yaw",
            "left_hip_roll", "left_hip_yaw", "left_hip_pitch",
            "left_knee_pitch", "left_ankle_pitch", "left_ankle_roll",
            "right_hip_roll", "right_hip_yaw", "right_hip_pitch",
            "right_knee_pitch", "right_ankle_pitch", "right_ankle_roll",
            "right_wrist_yaw", "left_wrist_yaw"]
    joint_values = [0.0]*len(joint_names)

    msg = {
            'joint_names': joint_names,
            'joint_positions': joint_values
          }

    while True:
        #joint_states_pub.publish(msg)
        time.sleep(2.0)

    zeno_py.shutdown()
