#!/usr/bin/env python

'''
An example of imu subscriber.

Author: Shehzad Ahmed
Date: 05.03.2016
'''

import time
# Import the modules we need
from qpid.messaging import *
from zeno_data_communication_manager.data_communication_manager import *

def torso_state_cb(msg):
    print msg
    print '=================================='

if __name__ == "__main__":
    broker = "192.168.188.27:5672"
    connection_options = {'sasl_mechanisms':'ANONYMOUS PLAIN MD5',
                          'username':'admin',
                          'password':'admin'}
    zeno_py = Node()
    zeno_py.init_node("imu_state_pub", broker,
                      connection_options=connection_options)
    
    torso_state_sub = Subscriber(zeno_py, "/robot_imu_interface/torso_state", "amq.topic", 1, torso_state_cb)

    while True:
        time.sleep(0.01)

    zeno_py.shutdown()
