/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface_test.cpp
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */

#include <iostream>
#include <robot_controller_interface/robot_controller_interface.h>

int main(int argc, char** argv) {

    double cycle_time = 1.0;
    
    std::string broker = "localhost:5672";

    RobotControllerInterface robot_controller_interface(broker, cycle_time);
    std::vector<std::string> names(26);
    std::vector<double> values;
    names = {"neck_roll", "neck_pitch", "neck_yaw", "waist",
            "left_shoulder_pitch", "left_shoulder_roll",
            "left_elbow_roll", "left_elbow_yaw",
            "right_shoulder_pitch", "right_shoulder_roll",
            "right_elbow_roll", "right_elbow_yaw",
            "left_hip_roll", "left_hip_yaw", "left_hip_pitch",
            "left_knee_pitch", "left_ankle_pitch", "left_ankle_roll",
            "right_hip_roll", "right_hip_yaw", "right_hip_pitch",
            "right_knee_pitch", "right_ankle_pitch", "right_ankle_roll",
            "right_wrist_yaw", "left_wrist_yaw"};
    values.resize(names.size());

    while(true) {
        std::vector<double> positions;

        robot_controller_interface.readJointPositions(names, positions);

        if (positions.size() == names.size()) {
            for(int i=0;i<positions.size();i++) {
                std::cout<< names.at(i) << ": " << positions.at(i) << ", ";
            }
            std::cout<< "\n===========================" << std::endl;
        }

        robot_controller_interface.sendPositionCommand(names, values);
        sleep(cycle_time);
    }

    return 0;
}
