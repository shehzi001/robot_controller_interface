# Important usage Instruction
This repository provide a robot controller interface.

# Pre-requisite installations:
  * qpid (download qpid-broker-0.16 and extract to e.g. /home/user_name/qpid-broker-0.16)
  * Java 1.6 or 1.7
  * Boost 1.45.0
  * CMake 2.8.7
  * Data Communication Manager

# Clone Data Communication Manager repository and follow the ReadMe for installation.

    git clone git@bitbucket.org:shehzi001/zeno_data_communication_manager.git

# Clone this repository and follow further instruction to install.

    git clone git@bitbucket.org:shehzi001/robot_controller_interface.git

# Installation

    cd robot_controller_interface/robot_controller_interface/ 
    mkdir build && cd  build
    cmake ..
    make
    sudo make install

# Test installation

    cd robot_controller_interface/robot_controller_interface_test
    mkdir build && cd build
    cmake ..
    make

## Running qpid broker

    Open new terminal:
    >> qpidd start
    Note: qpid-server will act as a server and listen on a specific port
          e.g. 5672.

## Examples

    Open new terminal:
    cd zeno_data_communication_manager/robot_controller_interface_test/build/
    >> ./robot_controller_interface_test

    Open new terminal:
    cd zeno_data_communication_manager/robot_controller_interface_test/examples/
    >> ./joint_states_publisher_test.py
