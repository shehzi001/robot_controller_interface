/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface.h
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */


#ifndef ROBOT_CONTROLLER_INTERFACE_H_
#define ROBOT_CONTROLLER_INTERFACE_H_

//----------------------------------------
// INCLUDES
#include <boost/shared_ptr.hpp>
#include <vector>
#include <iostream>
# include <chrono>


#include <zeno_data_communication_manager/node.h>
#include <zeno_data_communication_manager/publisher.h>
#include <zeno_data_communication_manager/subscriber.h>

#include <qpid/messaging/Message.h>
#include <qpid/types/Variant.h>

#include <boost/function.hpp>
#include <boost/thread.hpp>

using namespace qpid::messaging;
using namespace qpid::types;

using namespace std;
using  ns = chrono::nanoseconds;
using get_time = chrono::steady_clock;


/**
 * @brief The main class to provide interface to the robot joint controller 
    and sensor data.
 */
class RobotControllerInterface
{
    public:
        /**
         * Ctor.
         */
        RobotControllerInterface(const std::string &broker_address, const double &cycle_time);

        /**
         * Dtor.
         */
        ~RobotControllerInterface();

        /**
         * Sends position commands to joint position controller.
         */
        void sendPositionCommand(const std::vector<std::string> &joint_names, 
                                    const std::vector<double> &joint_positions);

        /**
         * Sends velocity commands to joint position controller.
         */
        void sendVelocityCommand(const std::vector<std::string> &joint_names,
                                    const std::vector<double> &joint_velocities);

        /**
         * Reads current joint positions.
         */
        bool readJointPositions(const std::vector<std::string> &joint_names, 
                                    std::vector<double> &joint_positions);

        /**
         * Reads current torso orientation feedback.
         */
        bool getTorsoFeedback(std::vector<double> &torso_feedback);

        /**
         *
         */
        void onJointStatesReceive(boost::function< void() > callbackFunc);


    private:
        /**
         * Callback for joint state
         */
        void jointStatescallback(qpid::messaging::Message message);

        /**
         * Callback to store torso state
         */
        void torsoStatecallback(qpid::messaging::Message message);

        /**
         * Utility method to prepare message to be published. 
         */
        Message prepareMessage(const std::vector<std::string> &names, 
                                                const std::vector<double> &values);

    private:
        /**
         * Copy Ctor.
         */
        RobotControllerInterface(const RobotControllerInterface &other);

        /**
         * Assignment operator
         */
        RobotControllerInterface &operator=(const RobotControllerInterface &other);

    private:
        /**
         * An interface to qpid dcm node.
         */
        DataCommunicationManager::Node node_handle_;

        /**()
         * Interface to qpid dcm Subscribers.
         */
        boost::shared_ptr<DataCommunicationManager::Subscriber> joint_states_sub_;
        boost::shared_ptr<DataCommunicationManager::Subscriber> torso_state_sub_;

        /**
         * Interface to qpid dcm Publishers.
         */
        boost::shared_ptr<DataCommunicationManager::Publisher> joint_position_command_pub_;
        boost::shared_ptr<DataCommunicationManager::Publisher> joint_velocity_command_pub_;

        /**
         * Stores joint states.
         */
        std::map<std::string, double> joint_states_map_;

        /**
         * Stores torso state.
         */
        std::vector<double> torso_state_;

        std::chrono::steady_clock::time_point start;

        boost::function<void(void)> on_joint_states_callback_;
};

#endif  // ROBOT_CONTROLLER_INTERFACE_H_
