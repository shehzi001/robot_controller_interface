/*
 * Copyright [2016] <Bonn-Rhein-Sieg University>
 *
 * robot_controller_interface.cpp
 *
 *  Created on: April 4, 2016
 *      Author: Shehzad Ahmed
 */

#include <robot_controller_interface/robot_controller_interface.h>

RobotControllerInterface::RobotControllerInterface(const std::string &broker_address, const double &cycle_time)
{
    std::string connectionOptions =  "{sasl_mechanisms:PLAIN,username:admin,password:admin}";

    node_handle_.initNode("robot_interface", broker_address, connectionOptions, true);

    std::string topic_type =  "amq.topic";

    node_handle_.setLoopTime(cycle_time);

    torso_state_sub_.reset(new DataCommunicationManager::Subscriber(
                                node_handle_, "/joint_controller/torso_state", topic_type,
                                boost::bind(&RobotControllerInterface::torsoStatecallback, this, _1 ) , 1)
                );

    joint_states_sub_.reset(new DataCommunicationManager::Subscriber(
                                node_handle_, "/joint_controller/joint_states", topic_type,
                                boost::bind(&RobotControllerInterface::jointStatescallback, this, _1 ) , 1)
                );

    joint_position_command_pub_.reset(new DataCommunicationManager::Publisher(
                                node_handle_, "/joint_controller/position_command", topic_type, 1)
                );
    joint_velocity_command_pub_.reset(new DataCommunicationManager::Publisher(
                                node_handle_, "/joint_controller/velocity_command", topic_type, 1)
                );

    torso_state_.resize(3);
    start = get_time::now(); //use auto keyword to minimize typing strokes :)
}

RobotControllerInterface::~RobotControllerInterface ()
{
}

void RobotControllerInterface::jointStatescallback(qpid::messaging::Message message)
{
    auto diff = get_time::now() - start;
    //cout<<"Elapsed time is :  "<< chrono::duration_cast<std::chrono::milliseconds>(diff).count()<<" ms "<<endl;

    Variant::Map joint_states;
    decode(message, joint_states);
    Variant::List joint_names = joint_states["joint_names"].asList();
    Variant::List joint_values = joint_states["joint_positions"].asList();


    if(!joint_names.empty() and !joint_values.empty() and 
        (joint_names.size()==joint_values.size())) {
        Variant::List::iterator i_names = joint_names.begin();
        Variant::List::iterator i_values = joint_values.begin();
        
        int iter = 0;
        joint_states_map_.clear();
        while(i_names != joint_names.end()) {
            joint_states_map_[(*i_names).asString()] = (*i_values).asDouble();
            i_values++;
            i_names++;
        }
    }
    start = get_time::now();
    if (!on_joint_states_callback_.empty())
        on_joint_states_callback_();
}

void RobotControllerInterface::torsoStatecallback(qpid::messaging::Message message)
{
    Variant::Map torso_state;
    decode(message, torso_state);
    Variant::List values = torso_state["angles"].asList();

    if(!values.empty() and (values.size()==torso_state_.size())) {
        Variant::List::iterator i;
        int iter = 0;
        for (i = values.begin(); i != values.end(); ++i) {
            torso_state_[iter] = (*i).asDouble();
            iter++;
        }
    }
}

void RobotControllerInterface::sendPositionCommand(const std::vector<std::string> &joint_names, 
                                            const std::vector<double> &joint_positions)
{
    joint_position_command_pub_->publish(prepareMessage(joint_names, joint_positions));
}

void RobotControllerInterface::sendVelocityCommand(const std::vector<std::string> &joint_names,
                                            const std::vector<double> &joint_velocities)
{
    joint_velocity_command_pub_->publish(prepareMessage(joint_names, joint_velocities));
}

bool RobotControllerInterface::readJointPositions(const std::vector<std::string> &joint_names, 
                                            std::vector<double> &joint_positions)
{
    if (joint_states_map_.empty())
        return false;

    for(int iter=0; iter<joint_names.size(); iter++) {
            std::map<std::string, double>::iterator it;
            it = joint_states_map_.find(joint_names[iter]);
            if (it != joint_states_map_.end()) {
                joint_positions.push_back(it->second);
            } else {
                joint_positions.push_back(0.0);
            }
    }
    //joint_states_map_.clear();

    return true;
}

bool RobotControllerInterface::getTorsoFeedback(std::vector<double> &torso_feedback)
{
    for(int i=0; i<torso_state_.size(); i++)
        torso_feedback.push_back(torso_state_[i]);
    return true;
}

Message RobotControllerInterface::prepareMessage(const std::vector<std::string> &names, const std::vector<double> &values)
{
    Variant::List names_list;
    for(int iter=0; iter<names.size(); iter++)
        names_list.push_back(Variant(names[iter]));

    Variant::List values_list;
    for(int iter=0; iter<values.size(); iter++)
        values_list.push_back(Variant(values[iter]));

    Variant::Map content;
    content["joint_names"] = names_list;
    content["joint_values"] = values_list;

    Message message;
    encode(content, message);

    return message;
}

void RobotControllerInterface::onJointStatesReceive(boost::function< void(void) > callbackFunc)
{
    on_joint_states_callback_ = boost::bind(callbackFunc);
}